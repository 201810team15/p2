package model.vo;

import java.text.SimpleDateFormat;

import model.comparators.ComparatorVOServicioDistancia;
import model.comparators.ComparatorVOServicioDuracion;


// TODO: Auto-generated Javadoc
/**
 * VO que representa un servicio de taxi realizado en la ciudad de Chicago.
 * @author jf.castaneda@uniandes.edu.co
 * @author md.galvan@uniandes.edu.co
 */
public class VOServicio {

	// ------------------------------------------------------------------------------------
	// ------------------------- Constantes -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/** The Constant FORMAT. */
	public static final SimpleDateFormat FORMAT = new SimpleDateFormat("YYYY-MM-DD'T'HH:mm:ss'.'000");
	
	/** The Constant INICIO_DE_LOS_TIEMPOS. */
	public static final long INICIO_DE_LOS_TIEMPOS = 0;
	
	/** The Constant FIN_DE_LOS_TIEMPOS. */
	public static final long FIN_DE_LOS_TIEMPOS = Long.MAX_VALUE;
	
	/** The Constant COMPARATOR_VOSERVICIO_DURACION. */
	public static final ComparatorVOServicioDuracion COMPARATOR_VOSERVICIO_DURACION = new ComparatorVOServicioDuracion();
	
	/** The Constant COMPARATOR_VOSERVICIO_DISTANCIA. */
	public static final ComparatorVOServicioDistancia COMPARATOR_VOSERVICIO_DISTANCIA = new ComparatorVOServicioDistancia();
	
	

	// ------------------------------------------------------------------------------------
	// ------------------------- Atributos ------------------------------------------------
	// ------------------------------------------------------------------------------------

	/** The id servicio. */
	private String idServicio;

	/** The duracion. En segundos.*/
	private double duracionEnSegundos;

	/** The distancia recorrida en Metros. */
	private double distanciaRecorridaEnMetros;

	/** The costo total. */
	private double costoTotal;	
	
	private double latitud;
	
	private double longuitud;
	
	private double peajes;
	
	// ------------------------------------------------------------------------------------
	// ------------------------- M�todo Constructor ---------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Instantiates a new VO servicio.
	 *
	 * @param idServicio the id servicio
	 * @param duracionEnSegundos the duracion en segundos
	 * @param distanciaRecorridaEnMetros the distancia recorrida en Metros
	 * @param costoTotal the costo total
	 */
	public VOServicio(String idServicio, double duracionEnSegundos, double distanciaRecorridaEnMetros, double costoTotal,
			double latitud, double longitud, double peajes) {
		super();
		this.idServicio = idServicio;
		this.duracionEnSegundos = duracionEnSegundos;
		this.distanciaRecorridaEnMetros = distanciaRecorridaEnMetros;
		this.costoTotal = costoTotal;
		this.latitud = latitud;
		this.longuitud = longitud;
		this.peajes = peajes;
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos --------------------------------------------------
	// ------------------------------------------------------------------------------------
	
	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLonguitud() {
		return longuitud;
	}

	public void setLonguitud(double longuitud) {
		this.longuitud = longuitud;
	}

	/**
	 * M�todo que retorna si la cantidad de puntos del servicio son v�lidos.
	 *
	 * @return true si s� son v�lidos 
	 */
	public boolean puntosValidos() {
		return duracionEnSegundos > 0 && distanciaRecorridaEnMetros > 0;
	}
	
	/**
	 * Gets the id servicio.
	 *
	 * @return the id servicio
	 */
	public String getIdServicio() {
		return idServicio;
	}

	/**
	 * Gets the duracion en segundos.
	 *
	 * @return the duracion en segundos
	 */
	public double getDuracionEnSegundos() {
		return duracionEnSegundos;
	}

	/**
	 * Gets the distancia recorrida en Metros.
	 *
	 * @return the distancia recorrida en Metros
	 */
	public double getDistanciaRecorridaEnMetros() {
		return distanciaRecorridaEnMetros;
	}

	/**
	 * Gets the costo total.
	 *
	 * @return the costo total
	 */
	public double getCostoTotal() {
		return costoTotal;
	}

	public double getPeajes() {
		return peajes;
	}

	public void setPeajes(double peajes) {
		this.peajes = peajes;
	}

	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	public void setDuracionEnSegundos(double duracionEnSegundos) {
		this.duracionEnSegundos = duracionEnSegundos;
	}

	public void setDistanciaRecorridaEnMetros(double distanciaRecorridaEnMetros) {
		this.distanciaRecorridaEnMetros = distanciaRecorridaEnMetros;
	}

	public void setCostoTotal(double costoTotal) {
		this.costoTotal = costoTotal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object servicio) {
		try {
			return this.idServicio.equals(((VOServicio)servicio).idServicio);
		}
		catch(ClassCastException pe) {
			return false;
		}
	}
}
