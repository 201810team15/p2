package model.vo;
import model.comparators.*;

//Se define que la comparacion natural se da por el promedio de la distacia recorrida, 
//en caso contrario se usaran los comparadores
public class VOPeso implements Comparable<VOPeso>{

	public final static ComparadorVOArcoTiempoServicio COMPARADOR_TIEMPO = new ComparadorVOArcoTiempoServicio();

	public final static ComparadorVOArcoValorPeaje COMPARADOR_VALOR_PEAJE = new ComparadorVOArcoValorPeaje();

	public final static ComparadorVOArcoValorSerivcio COMPARADOR_VALOR_SERV = new ComparadorVOArcoValorSerivcio();

	private double numServicios;

	private double distanciaRecorrida;

	private double valorServicio;

	private double valorPeaje;

	private double tiempoServicio;

	private String idCabeza;

	private String idCola;


	public VOPeso(VOServicio servicio, VOPuntoCiudad cabeza, VOPuntoCiudad cola){
		this.numServicios=1;
		this.distanciaRecorrida=0;
		this.valorPeaje = 0;
		this.valorServicio = 0;
		this.tiempoServicio = 0;
		this.idCabeza = cabeza.getIdVertice();
		this.idCola=cola.getIdVertice();
		actualizarPeso(servicio);
	}

	public VOPeso(double numServicios, double distanciaRecorrida, double valorSerivicio, double valorPeaje, 
			double tiempoServicio, String idCabeza, String idCola){

		this.numServicios= numServicios;
		this.distanciaRecorrida = distanciaRecorrida;
		this.valorServicio=valorSerivicio;
		this.valorPeaje = valorPeaje;
		this.tiempoServicio = tiempoServicio;
		this.idCabeza = idCabeza;
		this.idCola = idCola;
	}


	public String getIdCabeza() {
		return idCabeza;
	}


	public void setIdCabeza(String idCabeza) {
		this.idCabeza = idCabeza;
	}


	public String getIdCola() {
		return idCola;
	}


	public void setIdCola(String idCola) {
		this.idCola = idCola;
	}


	public double getDistanciaRecorrida() {
		return distanciaRecorrida;
	}



	public void setDistanciaRecorrida(double distanciaRecorrida) {
		this.distanciaRecorrida = distanciaRecorrida;
	}



	public double getValorServicio() {
		return valorServicio;
	}



	public void setValorServicio(double valorServicio) {
		this.valorServicio = valorServicio;
	}



	public double getValorPeaje() {
		return valorPeaje;
	}


	public double getNumServicios() {
		return numServicios;
	}


	public void setNumServicios(double numServicios) {
		this.numServicios = numServicios;
	}


	public double getTiempoServicio() {
		return tiempoServicio;
	}


	public void setTiempoServicio(double tiempoServicio) {
		this.tiempoServicio = tiempoServicio;
	}


	public void setValorPeaje(double valorPeaje) {
		this.valorPeaje = valorPeaje;
	}

	public void actualizarPeso(VOServicio servicio) {
		numServicios++;
		distanciaRecorrida=(distanciaRecorrida+servicio.getDistanciaRecorridaEnMetros())/numServicios;
		valorPeaje=(valorPeaje+servicio.getPeajes());
		valorServicio=(valorServicio+servicio.getCostoTotal())/numServicios;
		tiempoServicio=(tiempoServicio+servicio.getDuracionEnSegundos())/numServicios;
	}

	@Override
	public int compareTo(VOPeso o) {
		return Double.compare(distanciaRecorrida, o.distanciaRecorrida);
	}

}
