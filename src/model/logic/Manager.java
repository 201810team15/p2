package model.logic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.concurrent.SynchronousQueue;

import org.omg.CORBA.PUBLIC_MEMBER;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import api.IManager;
import model.data_structures.DiGraph;
import model.data_structures.RedBlackBST;
import model.data_structures.DiGraph.Vertex;
import model.data_structures.LinearProbingHashST;
import model.data_structures.LinkedList;
import model.data_structures.MinPQ;
import model.vo.VOPeso;
import model.vo.VOPuntoCiudad;
import model.vo.VOServicio;
import model.vo.VOPuntoCiudad;

public class Manager implements IManager{

	// ------------------------------------------------------------------------------------
	// ------------------------- Constantes
	// -----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Direcci�n del archivo Json peque�o.
	 */
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";

	/**
	 * Direcci�n del archivo Json mediano.
	 */
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";

	/**
	 * Direcci�n del archivo Json grande.
	 */
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	public static final String NULL_SERVICE_ID = "No Id for the service";

	private static final long MILISE_TO_SEC = (long)60*(long)15*(long)1000;

	private static final double MILE_TO_METER = 1609.34;
	
	private static final double NO_NUMERIC_DATA = Double.MAX_VALUE;

	private DiGraph<String, VOPuntoCiudad, VOPeso> grafoGenerico;

	private double distanciaDx;

	public Manager() {}





	@Override
	public void cargarJson(String direccionJson, double DistanciaDx) throws Exception {
		grafoGenerico = new DiGraph<String,VOPuntoCiudad,VOPeso>();
		this.distanciaDx = DistanciaDx;
		JsonParser parser = new JsonParser();
		int i =0;
		JsonArray array = (JsonArray) parser.parse(new FileReader(direccionJson));
		for (JsonElement jsonElement : array) {
			i++;
			JsonObject jsonObject = (JsonObject) jsonElement;
			VOServicio servicioPick = null;
			VOServicio servicioDrop = null;
			VOPuntoCiudad puntoPick = null;
			VOPuntoCiudad puntoDrop = null;
			VOPuntoCiudad puntoIterable = null;
			VOPeso pesoActual = null;
			Iterable<String> llavesGrafo = grafoGenerico.keys();
			double distanciaPick=-1,distanciaDrop=-1;

			// Id del servicio. Si existe, se instancia un servicio.
			String idServicio = jsonObject.get("trip_id") != null ? jsonObject.get("trip_id").getAsString() : "No TripID Available";

			// Duraci�n del servicio en segundos.
			int duracionEnSegundos = jsonObject.get("trip_seconds") != null? jsonObject.get("trip_seconds").getAsInt() : 0;

			// Distancia total recorrida durante el servicio.
			double distanciaRecorridaEnMetros = jsonObject.get("trip_miles") != null? jsonObject.get("trip_miles").getAsDouble() * MILE_TO_METER : 0;

			// Costo total del servicio.
			double costoTotal = jsonObject.get("trip_total") != null? jsonObject.get("trip_total").getAsDouble() : 0;

			//Costo de los pejaes

			double costoPeajes = jsonObject.get("tolls")!=null? jsonObject.get("tolls").getAsDouble():0;

			double latPick=0,lonPick=0,latDrop=0,lonDrop=0;

			//Cordenadas de pickup
			JsonObject pickup_centroid_location =(jsonObject.get("pickup_centroid_location")!=null)?jsonObject.get("pickup_centroid_location").getAsJsonObject():null;
			if(pickup_centroid_location!=null)
			{
				JsonArray coordArray = pickup_centroid_location.get("coordinates").getAsJsonArray();
				if(coordArray.get(0)!=null) latPick = coordArray.get(0).getAsDouble();
				if(coordArray.get(1)!=null) lonPick = coordArray.get(1).getAsDouble();
			}

			//Cordenadas de dropoff
			JsonObject dropoff_centroid_location =(jsonObject.get("dropoff_centroid_location")!=null)?jsonObject.get("dropoff_centroid_location").getAsJsonObject():null;
			if(dropoff_centroid_location!=null)
			{
				JsonArray coordArray = dropoff_centroid_location.get("coordinates").getAsJsonArray();
				if(coordArray.get(0)!=null) latDrop = coordArray.get(0).getAsDouble();
				if(coordArray.get(1)!=null) lonDrop = coordArray.get(1).getAsDouble();
			}

			if(latPick!=0 && lonPick!=0)
				servicioPick = new VOServicio(idServicio, duracionEnSegundos, distanciaRecorridaEnMetros, costoTotal, latPick, lonPick, costoPeajes);
			if(latDrop!=0 && lonDrop!=0)
				servicioDrop = new VOServicio(idServicio, duracionEnSegundos, distanciaRecorridaEnMetros, costoTotal, latDrop, lonDrop, costoPeajes);

			if(servicioPick!=null)
				puntoPick = new VOPuntoCiudad(servicioPick, true);
			if(servicioDrop!=null)
				puntoDrop = new VOPuntoCiudad(servicioDrop, false);

			boolean existeCercanoPick = false;
			boolean existeCercanoDrop = false;

			RedBlackBST<Double, VOPuntoCiudad> distanciasMenoresPick = new RedBlackBST<Double, VOPuntoCiudad>();  //Arbol para encontrar la llave en la menor distancia del vertice de comienzo
			RedBlackBST<Double, VOPuntoCiudad> distanciasMenoresDrop = new RedBlackBST<Double, VOPuntoCiudad>();  //Arbol para encontrar la llave en la menor distancia del vertice de fin


			for (String llave : llavesGrafo) {
				puntoIterable = grafoGenerico.getInfoVertex(llave);
				if(puntoPick!=null && puntoIterable!=null)
				{
					distanciaPick = distanciaVertices(puntoIterable, puntoPick);
					if(distanciaPick<=distanciaDx)
					{
						distanciasMenoresPick.put(distanciaPick, puntoIterable);
						existeCercanoPick=true;
					}
				}
				if(puntoDrop!=null && puntoIterable!=null)
				{
					distanciaDrop = distanciaVertices(puntoIterable, puntoDrop);
					if(distanciaDrop<=distanciaDx)
					{
						distanciasMenoresDrop.put(distanciaDrop, puntoIterable);
						existeCercanoDrop=true;
					}
				}
			}

			if(puntoPick!=null)
			{
				////System.out.println("UltraInstinto Pick1");
				if(existeCercanoPick)
				{
					////System.out.println("UltraInstinto Pick2");
					double llave = distanciasMenoresPick.min();
					////System.out.println(llave);
					VOPuntoCiudad puntoCercano = distanciasMenoresPick.get(llave);
					puntoCercano.agregarDatosVerticeCercanoCabeza(puntoPick);
					//System.out.println(puntoCercano.getIdVertice());
				}else
				{
					////System.out.println("UltraInstinto Pick3");
					grafoGenerico.addVertex(puntoPick.getIdVertice(), puntoPick);
				}
			}

			if(puntoDrop!=null)
			{
				////System.out.println("UltraInstinto Drop1");
				if(existeCercanoDrop)
				{
					////System.out.println("UltraInstinto Drop2");
					double llave = distanciasMenoresDrop.min();
					////System.out.println(llave);
					VOPuntoCiudad puntoCercano = distanciasMenoresDrop.get(llave);
					puntoCercano.agregarDatosVerticeCercanoCola(puntoDrop);
					//System.out.println(puntoCercano.getIdVertice());
				}else
				{
					////System.out.println("UltraInstinto Drop3");
					grafoGenerico.addVertex(puntoDrop.getIdVertice(), puntoDrop);
				}
			}

			String idPuntoCabeza = (puntoPick!=null)?puntoPick.getIdVertice():"";
			String idPuntoCola = (puntoDrop!=null)?puntoDrop.getIdVertice():"";
			if(distanciaPick<distanciaDx && distanciaPick>-1)
			{
				double llave = distanciasMenoresPick.min();
				idPuntoCabeza = distanciasMenoresPick.get(llave).getIdVertice();
			}
			if(distanciaDrop<distanciaDx && distanciaDrop>-1)
			{
				double llave = distanciasMenoresDrop.min();
				idPuntoCabeza = distanciasMenoresDrop.get(llave).getIdVertice();
			}

			if(!idPuntoCabeza.isEmpty() && !idPuntoCola.isEmpty() && servicioPick!=null)
			{
				VOPeso peso = grafoGenerico.getInfoArc(idPuntoCabeza, idPuntoCola);
				if(peso==null)
				{
					grafoGenerico.addEdge(idPuntoCabeza, idPuntoCola, new VOPeso(servicioPick, puntoPick, puntoDrop));
				}else
				{
					peso.actualizarPeso(servicioPick);
				}
			}


			////System.out.println("Llega al final");
		}
		//System.out.println(i);
	}




	private double distanciaVertices(VOPuntoCiudad puntoIterable, VOPuntoCiudad puntoReferencia){
		double lat2 = puntoIterable.getLatitud(), lng2 = puntoIterable.getLongitud(),
				lat1 = puntoReferencia.getLatitud(), lng1 = puntoIterable.getLongitud();
		double radioTierra = 6371*1000;//en metros  
		double dLat = Math.toRadians(lat2 - lat1);  
		double dLng = Math.toRadians(lng2 - lng1);  
		double sindLat = Math.sin(dLat / 2);  
		double sindLng = Math.sin(dLng / 2);  
		double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
		* Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));  
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
		double distancia = radioTierra * va2;  
		return distancia; 
	}


	private double distancia(double lat1, double lng1, double lat2, double lng2){
		double radioTierra = 6371*1000;//en metros  
		double dLat = Math.toRadians(lat2 - lat1);  
		double dLng = Math.toRadians(lng2 - lng1);  
		double sindLat = Math.sin(dLat / 2);  
		double sindLng = Math.sin(dLng / 2);  
		double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
		* Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));  
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
		double distancia = radioTierra * va2;  
		return distancia; 
	}

	@Override
	public int darNumeroVertices() {
		Iterable<String> llavesGrafo;
		int i = 1;
		llavesGrafo = grafoGenerico.keys();
		for (String llave : llavesGrafo) {
			//System.out.println("Vertice # " + i);
			VOPuntoCiudad actual = grafoGenerico.getInfoVertex(llave);
			//System.out.println(actual.getIdVertice());
			//								LinkedList<String> serCab = actual.getIdServiciosCabeza();
			//								LinkedList<String> serCo = actual.getIdServiciosCola();
			//								//System.out.println("ID servicios cabeza");
			//								for (String idServ : serCab) {
			//									//System.out.println(idServ);
			//								}
			//								//System.out.println("ID servicios cola");
			//								for (String idServ : serCo) {
			//									//System.out.println(idServ);
			//								}
			i++;
		}
		return grafoGenerico.V();
	}

	@Override
	public int darNumeroDeArcos() {
		return grafoGenerico.E();
	}

	/**
	 * M�todo que guarda la informaci�n del grafo en un archivo Json.
	 */
	public void persistir() throws IOException 
	{
		String dirreccion = "./Docs/JsonGrafo.json";
		File JSONFile = new File(dirreccion);
		BufferedWriter bw = null;
		try {
			if(!JSONFile.exists()) 
			{
				JSONFile.createNewFile();
				bw = new BufferedWriter(new FileWriter(JSONFile,false));
				//System.out.println("Se creo el archivo para persistir en JSON");
			}
			else 
			{
				bw = new BufferedWriter(new FileWriter(JSONFile,false));
				//System.out.println("Se encontro el archivo para persistir en JSON");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

		int i = 0;
		JsonArray data = new JsonArray();
		Iterable<String> llaves = grafoGenerico.keys();
		for (String llave : llaves) {
			i++;
			////System.out.println(llave);
			JsonObject vertice = new JsonObject();
			vertice.addProperty("key", llave);
			VOPuntoCiudad verticeActual = grafoGenerico.getInfoVertex(llave);
			JsonObject VOPuntoCiudad = new JsonObject();
			VOPuntoCiudad.addProperty("latitud", verticeActual.getLatitud());
			VOPuntoCiudad.addProperty("longitud", verticeActual.getLongitud());
			LinkedList<String> serviciosCabeza = verticeActual.getIdServiciosCabeza();
			LinkedList<String> serviciosCola = verticeActual.getIdServiciosCola();
			JsonArray idServiciosCabeza = new JsonArray();
			JsonArray idServiciosCola = new JsonArray();
			for (String servCa : serviciosCabeza) {
				idServiciosCabeza.add(servCa);
			}
			for (String servCo : serviciosCola) {
				idServiciosCola.add(servCo);
			}
			VOPuntoCiudad.add("idServiciosCabeza", idServiciosCabeza);
			VOPuntoCiudad.add("idServiciosCola", idServiciosCola);

			vertice.add("datosPuntoCiudad", VOPuntoCiudad);

			//Agrega los arcos que comienzen en ese punto
			Iterable<String> adj = grafoGenerico.adj(llave);
			VOPeso pesoActual = null;
			JsonArray pesos = new JsonArray();
			if(adj!=null){
				//System.out.println("Origen " +llave);
				for (String idVerticeAdjacente : adj) {
					JsonObject peso = new JsonObject();
					//System.out.println("Destino " + idVerticeAdjacente);
					pesoActual = grafoGenerico.getInfoArc(llave, idVerticeAdjacente);
					peso.addProperty("idComienzo", llave);
					peso.addProperty("idFin", idVerticeAdjacente);
					peso.addProperty("numero_de_servicios", pesoActual.getNumServicios());
					peso.addProperty("distancia_recorrida", pesoActual.getDistanciaRecorrida());
					peso.addProperty("valor_servicios", pesoActual.getValorServicio());
					peso.addProperty("valor_peajes", pesoActual.getValorPeaje());
					peso.addProperty("tiempo_servicio", pesoActual.getTiempoServicio());
					pesos.add(peso);
				}
			}
			vertice.add("arcos", pesos);
			data.add(vertice);

		}
		String datos = data.toString();
		bw.write(datos);
		bw.flush();
		bw.close();
		System.out.println("Se logro escribir " + i +" elementos" );
	}

	/**
	 * M�todo que carga la informaci�n del grafo desde el archivo Json.
	 */
	@SuppressWarnings("unchecked")
	public void antiPersistir() 
	{
		grafoGenerico= new DiGraph<String,VOPuntoCiudad,VOPeso>();
		JsonParser parser = new JsonParser();
		int i =0;
		try {
			JsonArray array = (JsonArray) parser.parse(new FileReader("./Docs/JsonGrafo.json"));
			for (JsonElement jsonElement : array) {
				JsonObject actual = (JsonObject) jsonElement;
				//Llave para vertice
				String llave = actual.get("key").getAsString();
				JsonObject puntoCiudadActual = actual.getAsJsonObject("datosPuntoCiudad");
				double latitud = puntoCiudadActual.get("latitud").getAsDouble();
				double longitud = puntoCiudadActual.get("longitud").getAsDouble();
				JsonArray idServiciosCabeza = puntoCiudadActual.getAsJsonArray("idServiciosCabeza");
				LinkedList<String> idServCa = new LinkedList<String>();
				for (JsonElement idServicios : idServiciosCabeza) {
					idServCa.add(idServicios.getAsString());
				}
				JsonArray idServiciosCola = puntoCiudadActual.getAsJsonArray("idServiciosCola");
				LinkedList<String> idServCo = new LinkedList<String>();
				for (JsonElement idServicios : idServiciosCola) {
					idServCo.add(idServicios.getAsString());
				}
				//Se agrega el vertice
				VOPuntoCiudad puntoCiudad = new VOPuntoCiudad(idServCa, idServCo, latitud, longitud, llave);
				grafoGenerico.addVertex(llave, puntoCiudad);
				
				//Se comienzan a crear los arcos asociados a ese vertice
				JsonArray arcos = actual.getAsJsonArray("arcos");
				for (JsonElement arco : arcos) {
					i++;
					JsonObject arcoActual = (JsonObject) arco;
					String idComienzo = arcoActual.get("idComienzo").getAsString();
					String idFin = arcoActual.get("idFin").getAsString();
					double numServicios = arcoActual.get("numero_de_servicios").getAsDouble();
					double distanciaRecorria = (arcoActual.get("distancia_recorrida").getAsDouble()!=0)?arcoActual.get("distancia_recorrida").getAsDouble():NO_NUMERIC_DATA;
					double valorServicios = (arcoActual.get("valor_servicios").getAsDouble()!=0)?arcoActual.get("valor_servicios").getAsDouble():NO_NUMERIC_DATA;
					double valorPeajes = arcoActual.get("valor_peajes").getAsDouble();
					double tiempoServicios = arcoActual.get("tiempo_servicio").getAsDouble();
					VOPeso pesoActual = new VOPeso(numServicios, distanciaRecorria, valorServicios, valorPeajes, tiempoServicios, idComienzo, idFin);
					grafoGenerico.addEdge(idComienzo, idFin, pesoActual);
				}
			}
			System.out.println(i);
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}


