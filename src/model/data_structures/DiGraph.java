package model.data_structures;

import java.util.Comparator;
import java.util.NoSuchElementException;

import com.google.gson.Gson;


public class DiGraph<K,V,A extends Comparable<A>> implements IDigraph<K, V, A> {

	public final static String VERTEX_ID_NULL = "VERTEX_ID_NULL";

	private LinearProbingHashST<K, Vertex<K, V>> vertices;
	
	private LinearProbingHashST<K, MinPQ<Edge<A>>> arcos; //La llave es el vertice de origen, el valor 
	//es una lista de los arcos de ese vertice 
	
	private int V;
	
	private int E;

	private Comparator<A> comparador; //El comparador es nulo si se asume que A es comparable

	public DiGraph() {
		this(null);
	}

	public DiGraph(Comparator<A> comparador)
	{
		V=0;
		E=0;
		this.comparador = comparador;
		vertices = new LinearProbingHashST<K,Vertex<K,V>>();
		arcos = new LinearProbingHashST<K, MinPQ<Edge<A>>>();
	}

	@Override
	public int V() {
		return V;
	}

	@Override
	public int E() {
		return E;
	}

	@Override
	public void addVertex(K idVertice, V infoVertex) {
		if(idVertice==null || infoVertex==null) 
			throw new IllegalArgumentException(VERTEX_ID_NULL);
		else
		{
			if(!vertices.contains(idVertice))
			{
				MinPQ<Edge<A>> arcosVertice=null;
				Vertex<K,V> vertice = new Vertex<K, V>(idVertice, infoVertex);
				arcosVertice = new MinPQ<Edge<A>>();
				vertices.put(idVertice, vertice);
				arcos.put(idVertice, arcosVertice);
				V++;
			}
			else
			{
				vertices.put(idVertice, new Vertex<K, V>(idVertice, infoVertex));				
			}
		}
	}

	@Override
	public void addEdge(K idVertexIni, K idVertexFin, A infoArc) {
		//Excepcion si alguno de los parametros es nulo 
		if(idVertexIni==null || idVertexFin==null || infoArc==null)
			throw new IllegalArgumentException(VERTEX_ID_NULL);

		// Se buscan los vertices de origen y fin 
		Vertex<K,V> origen = vertices.get(idVertexIni);
		Vertex<K,V> fin = vertices.get(idVertexFin);
		if(origen!=null && fin!=null) {
			try {
				Edge<A> newArco = new Edge<A>(origen, fin,infoArc); //Aca manda assertion error si no se cumple con las invariantes
				arcos.get(idVertexIni).insert(newArco);
				E++;
			} catch (AssertionError e) {
				if(origen==null)
					throw new NoSuchElementException("El origen del arco no existe");
				else if(fin==null)
					throw new NoSuchElementException("El fin del arco no existe");
				else
					throw new NoSuchElementException("El origen y el fin del arco no existe");
			}
		}
	}

	@Override
	public V getInfoVertex(K IdVertex) {
		//Excepcion si alguno de los parametros es nulo 
		if(IdVertex==null)
			throw new IllegalArgumentException("El parametro no puede ser nulo");

		Vertex<K,V> vertice = vertices.get(IdVertex);
		if(vertice==null)
			throw new NoSuchElementException("No existe el vertice con esa id");
		else
			return vertice.getInfoVertice();
	}

	@Override
	public void setInfoVertex(K idVertex, V infoVertex) {
		if(idVertex==null)
			throw new IllegalArgumentException(VERTEX_ID_NULL);
		Vertex<K,V> vertice = vertices.get(idVertex);
		if(vertice==null)
			throw new NoSuchElementException("No existe el vertice con esa id");
		else
			vertice.setInfoVertice(infoVertex);
	}

	@Override
	public A getInfoArc(K idVertexIni, K idVertexFin){
		//Exception si algun parametro es nulo
		if(idVertexIni==null || idVertexFin==null)
			throw new IllegalArgumentException("Ninguno de los parametros puede ser nulo");

		//Se obtienen los vertices necesarios
		Vertex<K,V> inicio = vertices.get(idVertexIni);
		Vertex<K,V> fin = vertices.get(idVertexFin);

		//Exception si alguno de los vertices es nulo;
		if(inicio==null)
			return null;
		else if(fin==null)
			return null;
		MinPQ<Edge<A>> arcosVertice = arcos.get(idVertexIni);

		//Exception si la lista de arcos esta vacia (Para que no se putee con los foreach)
		if(arcosVertice.size()==0)
			return null;

		//Se busca el arco correcto
		for (Edge<A> edge : arcosVertice) 
		{

			if(edge.cola.equals(idVertexFin))
			{
				return edge.infoEdge;
			}
		}
		return null;
	}

	public void setVertices(LinearProbingHashST<K, Vertex<K, V>> vertices) {
		this.vertices = vertices;
	}

	public void setArcos(LinearProbingHashST<K, MinPQ<Edge<A>>> arcos) {
		this.arcos = arcos;
	}

	@Override
	public void setInforArc(K idVertexIni, K idVertexFin, A infoArc) {
		if(idVertexIni==null || idVertexFin==null)
			throw new IllegalArgumentException("Ninguno de los parametros puede ser nulo");

		//Se obtienen los vertices necesarios
		Vertex<K,V> inicio = vertices.get(idVertexIni);
		Vertex<K,V> fin = vertices.get(idVertexFin);

		//Exception si alguno de los vertices es nulo;
		if(inicio==null)
			throw new NoSuchElementException("El origen del arco no existe");
		else if(fin==null)
			throw new NoSuchElementException("El fin del arco no existe");

		MinPQ<Edge<A>> arcosVertice = arcos.get(idVertexIni);

		//Exception si la no existe lista de arcos para ese vertice
		if(arcosVertice==null)
			throw new NoSuchElementException("No existe arco para ese vertice");

		//Exception si la lista de arcos esta vacia (Para que no se putee con los foreach)
		if(arcosVertice.size()==0)
			return;

		//Se busca el arco correcto
		for (Edge<A> edge : arcosVertice) {
			if(edge.cola.equals(idVertexFin))
				edge.setInfoEdge(infoArc);
		}
	}

	@Override
	public Iterable<K> adj(K idVertex) {
		if(idVertex==null)
			throw new IllegalArgumentException("El parametro no puede ser nulo");
		MinPQ<Edge<A>> arcosVertice = arcos.get(idVertex);
		if(arcosVertice==null)
			throw new NoSuchElementException("No existe la lista de arcos para ese vertice");
		if(arcosVertice.size()==0)
			return null;
		Queue<K> idVertices = new Queue<K>();
		for (Edge<A> arcos :arcosVertice ) {
			idVertices.enqueue(arcos.cola);
		}
		return idVertices;	
	}

	public Iterable<K> keys(){
		return vertices.keys();
	}
	
	public class Edge<A extends Comparable<A>> implements Comparable<Edge<A>>{
		private A infoEdge;  //Informacion del vertice
		private K cabeza;  // Comienzo del arco
		private K cola;  //Fin del arco
		private Comparator<A> comparadorEdge;

		public Edge(Vertex<K,V> cabeza, Vertex<K,V> cola, A infoEdge){
			this.infoEdge = infoEdge;
			this.cabeza = cabeza.getIdVertice();
			this.cola = cola.getIdVertice();
			this.comparadorEdge = (Comparator<A>) comparador;
			check();
		}

		public void setInfoEdge(A infoEdge) {
			this.infoEdge = infoEdge;
		}

		/**
		 * set de la cola del vertice
		 * @return
		 */
		public void setCola(K cola) {
			this.cola = cola;
		}

		private void check(){
			assert(cabeza!=null);
			assert(cola!=null);
		}

		public String toString(){
			return "Cabeza: " + cabeza+"//Cola: " + cola ;

		}

		@Override
		public int compareTo(Edge<A> o) {
			if(comparadorEdge==null)
				return infoEdge.compareTo(o.infoEdge);
			else
				return comparadorEdge.compare(infoEdge, o.infoEdge);
		}

	}
	public class Vertex<K,V>{

		private K idVertice;
		private V infoVertice;

		public Vertex(K idVertice, V infoVertice){
			this.idVertice = idVertice;
			this.infoVertice = infoVertice;
		}

		public K getIdVertice() {
			return idVertice;
		}

		public V getInfoVertice() {
			return infoVertice;
		}
		public void setInfoVertice(V infoVertice) {
			this.infoVertice = infoVertice;
		}			
	}
}
