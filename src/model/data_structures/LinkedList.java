package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;


// TODO: Auto-generated Javadoc
/**
 * Clase que representa la LinkedLista encadenada gen�rica a utilizar. <br>
<<<<<<< HEAD
 * Modificado el nombre de la estructura de Lista a LinkedList.
=======
 *
 * Modificado el nombre de la estructura de LinkedLista a LinkedList.
>>>>>>> 59471e478eaa2a4888c79d16ce7b8b733f01d809
 * @param <E> the element type
 */
public class LinkedList<E> implements Iterable<E> {

	// ------------------------------------------------------------------------------------
	// ------------------------- Atributos ------------------------------------------------
	// ------------------------------------------------------------------------------------

	/** Nodo inicio de la LinkedLista. */
	private Node<E> primero;

	/** Nodo final de la LinkedLista. */
	private Node<E> ultimo;

	/** Tama�o actual de la LinkedLista. */
	private int tam;

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todo Constructor ---------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Crea una nueva LinkedLista gen�rica. <br>
	 * <b> post: </b> Se cre� una LinkedLista encadenada gen�rica vac�a.
	 */
	public LinkedList() {
		primero = null;
		ultimo = null;
		tam = 0;
	}

	/**
	 * Instantiates a new linked list.
	 *
	 * @param data the data
	 */
	public LinkedList(E data){
		this();
		addFirst(data);
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos --------------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Agrega un objeto al principio de la LinkedLista.
	 *
	 * @param data the data
	 */
	public void addFirst(E data) {
		Node<E> n = new Node<E>(data);
		if (tam == 0) {
			primero = n;
			ultimo = n;
		} else {
			n.setNext(primero);
			primero = n;
		}
		tam++;
	}

	/**
	 * Agrega un objeto al final de la LinkedLista.
	 *
	 * @param data the data
	 */
	public void add(E data) {
		Node<E> n = new Node<E>(data);
		if (tam == 0) {
			primero = n;
			ultimo = n;
		} else {
			ultimo.setNext(n);
			ultimo = ultimo.getNext();
		}
		tam++;
	}

	/**
	 * Retorna el primer nodo de la LinkedLista.
	 * @return primero, primer nodo de la LinkedLista
	 */
	public E getFirst() {
		if(primero!=null){
			return primero.getValue();
		}
		return null;
	}

	/**
	 * Retorna el �ltimo nodo de la LinkedLista.
	 * @return �ltimo, �ltimo nodo de la LinkedLista
	 */
	public E getLast() {
		return ultimo.getValue();
	}

	/**
	 * Removes the elemento.
	 * @param elemento the elemento
	 * @return the e
	 */
	public E remove(E elemento){
		E rta = null;
		if(elemento.equals(primero.getValue())){
			rta=primero.getValue();
			primero = primero.getNext();
			tam--;
		}
		else{
			Node<E> actual = primero;
			boolean encontro = false;
			while(!encontro && actual!=null && actual.getNext() !=null){
				if(actual.getNext().getValue().equals(elemento)){
					encontro=true;
					tam--;
				}
				else{
					actual = actual.getNext();
				}
			}
			if(encontro){
				Node<E> temp =actual.getNext();
				rta=temp.getValue();
				actual.setNext(temp.getNext());
			}
		}

		return rta;
	}

	/**
	 * Elimina y retorna el primer elemento de la LinkedLista.
	 * @return primer elemento de la LinkedLista.
	 */
	public E removeFirst() {
		E data = primero.getValue();
		primero = primero.getNext();
		tam--;
		return data;
	}

	/**
	 * Devuelve el tama�o actual de la LinkedLista.
	 * @return tama�o actual.
	 */
	public int size() {
		return tam;
	}

	/**
	 * Devuelve si la LinkedLista se encuentra actualmente vac�a.
	 * @return true si la LinkedLista se encuentra vac�a, false de lo contrario.
	 * Devuelve si la LinkedLista se encuentra actualmente vacía.
	 * true si la LinkedLista se encuentra vacía, false de lo contrario.
	 */
	public boolean isEmpty() {
		return (tam == 0);
	}

	/**
	 * Devuelve un iterador para la LinkedLista.
	 * @return iterator.
	 */
	public Iterator<E> iterator() {
		return new Iterador();
	}

	/**
	 * M�todo que ordena la LinkedLista utilizando MergeSort.
	 * @param c the c
	 */
	public void sort(Comparator<E> c) {
		LinkedList<E> l = sort(c, this);
		ultimo = l.ultimo;
		primero = l.primero;
	}

	/**
	 * M�todo recursivo para hacer el sort de Mergesort.
	 * @param c the c
	 * @param l the l
	 * @return the linked list
	 */
	public LinkedList<E> sort(Comparator<E> c, LinkedList<E> l) {
		if (l.size() <= 1) {
			return l;
		} else {
			LinkedList<E> l1 = new LinkedList<E>();
			LinkedList<E> l2 = new LinkedList<E>();
			Iterator<E> i_s = l.iterator();
			Iterator<E> i_f = l.iterator();
			int n = 0;
			while (i_f.hasNext()) {
				i_f.next();
				if (n % 2 == 0) {
					l1.add(i_s.next());
				}
				n++;
			}
			while (i_s.hasNext()) {
				l2.add(i_s.next());
			}
			l1 = sort(c, l1);
			l2 = sort(c, l2);
			l = merge(l1, l2, c);
			return l;
		}
	}

	/**
	 * M�todo que hace el merge de MergeSort.
	 * @param l1 the l 1
	 * @param l2 the l 2
	 * @param c the c
	 * @return the linked list
	 */
	private LinkedList<E> merge(LinkedList<E> l1, LinkedList<E> l2, Comparator<E> c) {
		LinkedList<E> rta = new LinkedList<E>();
		while (!l1.isEmpty() && !l2.isEmpty()) {
			int n = c.compare(l1.getFirst(), l2.getFirst());
			if (n <= 0) {
				rta.add(l1.removeFirst());
			} else {
				rta.add(l2.removeFirst());
			}
		}
		while (!l1.isEmpty()) {
			rta.add(l1.removeFirst());
		}
		while (!l2.isEmpty()) {
			rta.add(l2.removeFirst());
		}
		return rta;
	}

	/**
	 * Contains.
	 * @param t the t
	 * @return true, if successful
	 */
	public boolean contains(E t){
		boolean rta=false;
		Node<E> actual = primero;
		while(!rta &&  actual!=null){
			if(t.equals(actual.getValue())){
				rta=true;
			}
			actual=actual.getNext();
		}
		return rta;
	}

	/**
	 * Equals.
	 * @param LinkedLista the linked lista
	 * @return true, if successful
	 */
	public boolean equals(LinkedList<E> LinkedLista){
		if(LinkedLista==null){
			return false;
		}
		boolean rta = tam==LinkedLista.tam;
		if(rta){
			Iterator<E> i1 = this.iterator();
			Iterator<E> i2 = LinkedLista.iterator();
			while(i1.hasNext() && rta){
				E e1 = i1.next();
				E e2 = i2.next();
				rta = e1.equals(e2);
			}
		}
		return rta;
	}


	public LinkedList<E> invertList(){
		LinkedList<E> rta = new LinkedList<E>();
		Node<E> actual = primero;
		while(actual!=null){
			rta.addFirst(actual.getValue());
			actual = actual.getNext();
		}
		return rta;
	}



	/**
	 * Adds the sorted.
	 * @param elemento the elemento
	 * @param c the c
	 */
	protected void addSorted(E elemento, Comparator<E> c){

		if(primero==null || c.compare(elemento, primero.getValue())>=0){
			addFirst(elemento);
		}
		else{
			Node<E> n = new Node<E>(elemento);
			Node<E> actual = primero;
			boolean agrego = false;
			while(!agrego){
				if(actual.getNext()==null){
					actual.setNext(n);
					agrego=true;
					tam++;
				}
				else if(c.compare(elemento, actual.getNext().getValue())>=0){
					n.setNext(actual.getNext());
					actual.setNext(n);
					agrego=true;
					tam++;
				}
				actual=actual.getNext();
			}
		}
	}

	private class Iterador implements Iterator<E> {
		private Node<E> current = primero;

		public boolean hasNext()  { return current != null;                     }
		public void remove()      { throw new UnsupportedOperationException();  }

		public E next() {
			if (!hasNext()) throw new NoSuchElementException();
			E item = current.getValue();
			current = current.getNext(); 
			return item;
		}
	}

	/**
	 * The Class Node.
	 * @param <T> the generic type
	 */
	private class Node <E> {

		// ------------------------------------------------------------------------------------
		// ------------------------- Atributos ------------------------------------------------
		// ------------------------------------------------------------------------------------

		/** The next. */
		private Node<E> next;

		/** The prev. */
		private Node<E> prev;

		/** The value. */
		private E value;

		// ------------------------------------------------------------------------------------
		// ------------------------- M�todo Constructor ---------------------------------------
		// ------------------------------------------------------------------------------------

		/**
		 * Instantiates a new node.
		 */
		public Node(){
			this.next = null;
			this.prev = null;
			this.value= null;
		}

		/**
		 * Instantiates a new node.
		 * @param value the value
		 */
		public Node(E value) {
			this.next = null;
			this.prev = null;
			this.value = value;
		}

		// ------------------------------------------------------------------------------------
		// ------------------------- M�todos --------------------------------------------------
		// ------------------------------------------------------------------------------------

		/**
		 * Gets the next.
		 * @return the next
		 */
		public Node<E> getNext() {
			return next;
		}

		/**
		 * Sets the next.
		 * @param next the new next
		 */
		public void setNext(Node<E> next) {
			this.next = next;
		}

		/**
		 * Gets the prev.
		 * @return the prev
		 */
		public Node<E> getPrev() {
			return prev;
		}

		/**
		 * Sets the prev.
		 * @param prev the new prev
		 */
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

		/**
		 * Gets the value.
		 * @return the value
		 */
		public E getValue() {
			return value;
		}

		/**
		 * Sets the value.
		 * @param value the new value
		 */
		public void setValue(E value) {
			this.value = value;
		}
	}

}