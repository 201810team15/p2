package model.comparators;

import java.util.Comparator;

import model.vo.VOPeso;

public class ComparadorVOArcoValorSerivcio implements Comparator<VOPeso> {

	@Override
	public int compare(VOPeso arg0, VOPeso arg1) {
		if(arg0.getValorServicio()>arg1.getValorServicio())
			return 1;
		else if(arg0.getValorServicio()<arg1.getValorServicio())
			return -1;
		else
			return 0;
	}

}