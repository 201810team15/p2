package api;

import java.io.IOException;

import model.data_structures.DiGraph;
import model.vo.VOPeso;

public interface IManager {

	// ------------------------------------------------------------------------------------
		// ------------------------- M�todos --------------------------------------------------
		// ------------------------------------------------------------------------------------

		/**
		 * M�todo que carga los archivos Json.
		 * @param direccionJson
		 * @throws Exception si hay una falla durante la carga del archivo.
		 */
		public void cargarJson(String direccionJson, double DistanciaDx) throws Exception; 
		
		public int darNumeroVertices();
		
		public int darNumeroDeArcos();
		
		public void persistir() throws IOException;
		
		public void antiPersistir();
}
