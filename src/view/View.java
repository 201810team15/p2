package view;

import java.io.IOException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;


import controller.Controller;
import model.logic.Manager;
import model.data_structures.*;
import model.vo.*;

/**
 * Vista de la aplicaci�n.
 * 
 * @author jf.castaneda@uniandes.edu.co
 * @author md.galvan@uniandes.edu.co
 */
public class View {

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todo Main
	// ----------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * M�todo main de la aplicaci�n.
	 */
	public static void main(String[] args) {

		String json = "";
		double distanciaDx=0;
		// Creaci�n del escaner.
		Scanner sc = new Scanner(System.in);

		// Boolean que representa el estado del programa.
		boolean activo = true;

		// Boolean que representa si ya hay informaci�n cargada en el sistema.
		boolean datosCargados = false;

		// Ciclo del programa mientras est� activo.
		while (activo) {

			// Ejecuci�n de la interfaz principal del men�.
			menu();

			// Ejecuta si los datos no han sido cargados.
			if (!datosCargados) {

				// Impresi�n del men� cuando no se han cargado datos.
				menuSinDatos();

				// Lectura de la opci�n que n�mero ingres�.
				int opcion = sc.nextInt();


				// Switch para saber qu� json eligi�.
				switch (opcion) {

				// El usuario eligi� el archivo peque�o.
				case 1:
					json = Manager.DIRECCION_SMALL_JSON;
					break;

					// El usuario eligi� el archivo mediano.
				case 2:
					json = Manager.DIRECCION_MEDIUM_JSON;
					break;

					// El usuario eligi� el archivo grande.
				case 3:
					json = Manager.DIRECCION_LARGE_JSON;
					break;

					// El usuario eligi� un comando inv�lido.
				default:
					System.out.println("Ingres� un n�mero inv�lido.");
				}

				// L�nea que se ejecuta si el comando que ingres� el usuario es
				// v�lido.
				if (!json.equals("")) {

					menuDistancias();
					opcion = sc.nextInt();
					switch (opcion) {
					case 1:
						distanciaDx=25;
						break;
					case 2:
						distanciaDx=50;
						break;
					case 3:
						distanciaDx=75;
						break;
					case 4:
						distanciaDx=100;
						break;
					case 5:
						distanciaDx=250;
						break;
					case 6:
						distanciaDx=500;
						break;
					case 7:
						distanciaDx=750;
						break;
					default:
						System.out.println("Ingres� un n�mero inv�lido.");
						break;
					}

					// Los datos fueron exitosamente cargados.
					datosCargados = true;

					// Memoria y tiempo antes de cargar el archivo.
					long memoriaInicio = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
					long tiempoInicio = System.nanoTime();

					// Carga de los datos.
					try {
						Controller.cargarJson(json, distanciaDx);
					} catch (Exception e) {
						e.printStackTrace();
						break;
					}

					// Tiempo que se demor� cargando los datos.
					long tiempoFin = System.nanoTime();
					long duracion = (tiempoFin - tiempoInicio) / (1000000);

					// Memoria usada cargando los datos.
					long memoriaFin = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();

					// Mensaje de cu�nto tiempo se gast� durante el m�todo y
					// cu�nta memoria gast�.
					System.out.println("Tiempo que demor� en cargar: " + duracion
							+ " milisegundos \nMemoria utilizada durante la carga:  "
							+ ((memoriaFin - memoriaInicio) / 1000000.0) + " MB");

				}
			}

			// Ejecuta si los datos ya fueron cargados.
			else {

				// Impresi�n del men� cuando se cargaron los datos.
				menuConDatos();

				// Lectura de la opci�n que n�mero ingres�.
				int opcion = sc.nextInt();

				// Switch para saber qu� json eligi�.
				switch (opcion) {

				// El usuario eligi� la opci�n de salir.
				case 1:
					System.out.println("Hasta luego.");
					activo = false;
					sc.close();
					break;

					// El usuario eligi� la opci�n de cargar un nuevo archivo.
				case 2:
					datosCargados = false;
					json="";
					distanciaDx=0;
					break;

					// El usuario elige el requerimiento 1A.
				case 3:
					System.out.println(Controller.E());
					break;

					// El usuario eelige el requerimiento 2A.
				case 4:
					System.out.println(Controller.V());
					break;
				case 5:
					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
					LocalDateTime now = LocalDateTime.now();
					String FechaActual=dtf.format(now);
					String separador ="//////////////////////////////  \n";
					System.out.println("Se sobre-escribira el archivo con la siguiente informacion y formato:");
					System.out.println("Fecha y Hora de creacion: " + FechaActual);
					System.out.println("Numero de vertices: " + Controller.V());
					System.out.println("Numero de Arcos: " + Controller.E());
					System.out.println("Tama�o del archivo" + json);
					System.out.println("Distancia Dx: " +distanciaDx);
					String write = "Fecha y Hora de creacion: " + FechaActual + "\n" +
							"Numero de vertices: " + Controller.V() + "\n" +
							"Numero de Arcos: " + Controller.E()  + "\n" +
							"Tama�o del archivo" + json +"\n" +
							"Distancia Dx: " +distanciaDx +"\n";
					String dirreccion = "./Docs/Leame.txt";
					File Leame = new File(dirreccion);
					try {
						if(!Leame.exists()) 
						{
							Leame.createNewFile();
							BufferedWriter bw = new BufferedWriter(new FileWriter(Leame, true));
							bw.write(separador);
							bw.write(write);
							bw.write(separador);
							bw.close();
							System.out.println("Archivo escrito");
						}
						else 
						{
							BufferedWriter bw = new BufferedWriter(new FileWriter(Leame, true));
							bw.write(write);
							bw.write(separador);
							bw.close();
							System.out.println("Archivo escrito");
						}
					}catch (Exception e) {
						e.printStackTrace();
					}
					break;

				case 6:

					try
					{
						Controller.persistir();	
					}
					catch(IOException ioe)
					{
						System.out.println(ioe.getMessage());
					}
					break;
				case 7:

					Controller.antiPersistir();
					break;
					// El usuario eligi� un comando inv�lido.
				default:
					opcion = -1;
					System.out.println("Ingres� un n�mero inv�lido.");
					break;
				}
			}
		}
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos
	// --------------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * M�todo que imprime el men� de la aplicaci�n
	 */
	private static void menu() {
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("------------------------- Taxis de Chicago. Versi�n 2.0 ----------------------------");
		System.out.println("------------------------------------------------------------------------------------");
		System.out.println("");
	}

	/**
	 * M�todo que imprime el resto del men� si la aplicaci�n tiene datos
	 * cargados.
	 */
	private static void menuConDatos() {
		System.out.println("1. Salir de la app.");
		System.out.println("2. Cargar nuevo archivo.");
		System.out.println("3. E()");
		System.out.println("4. V()");
		System.out.println("5. escribir archivo txt");
		System.out.println("6. Crear archivo JSON");
		System.out.println("7. Cargar archivo JSON");
	}

	/**
	 * M�todo que imprime el resto del men� si la aplicaci�n no tiene datos
	 * cargados.
	 */
	private static void menuSinDatos() {
		System.out.println("Seleccione el archivo que quiere cargar:");
		System.out.println(" 1. Peque�o");
		System.out.println(" 2. Mediano");
		System.out.println(" 3. Grande");
	}


	/**
	 * M�todo que imprime las distancias disponibles
	 * cargados.
	 */
	private static void menuDistancias() {
		System.out.println("Seleccione la distancia Dx para crear el grafo [Estas distancias estan en metros]:");
		System.out.println("1. 25");
		System.out.println("2. 50");
		System.out.println("3. 75");
		System.out.println("4. 100");
		System.out.println("5. 250");
		System.out.println("6. 500");
		System.out.println("7. 750");
	}
}
